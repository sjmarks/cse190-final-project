"""
Authors
Samuel Marks, A10568175
Bao Liang, A99089908
"""

from operator import add #Used for mapping

"""
Name: astar
Description: method implementing A* algorithm
"""
def astar(move_list, start, goal, walls, pits):
    open_set = [[start]]   # List of paths currently discovered (list of list of cells)
    while open_set:
        paths = []  # Create a list of new paths
        # Loop through all the paths currently in the open set
        for path in open_set:
            nodes = []  # List of nodes that can be explored for this path
            # Go through all the moves in the list
            for move in move_list:
                curr_node = map(add, move, path[-1])
                if curr_node not in walls and curr_node not in pits and curr_node not in path:
                    nodes.append(curr_node)
            # Loop through the nodes discovered for this path
            for node in nodes:
                new_path = path + [node]
                # Check if new node is the goal
                if node == goal:
                    return new_path
                # append to the paths otherwise
                paths.append(new_path)
        # Set the open set equal to the list of new paths
        open_set = paths
    return []
