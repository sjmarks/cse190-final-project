#!/usr/bin/env python

"""
Authors
Samuel Marks, A10568175
Bao Liang, A99089908
"""

import rospy
from astar import astar
import mdp
import mbl

from copy import deepcopy as dp
from read_config import read_config
from cse_190_assi_3.msg import *
from std_msgs.msg import Bool

class Robot():
    def __init__(self):
        self.init_config_vals()
        self.init_ros_stuff()
        astar_path = self.astar_fn()
        rospy.sleep(1)
        self.pub_astar(astar_path)
        self.run_mdp()
        rospy.sleep(1)
        self.run_mbl()
        self.shutdown()

    def astar_fn(self):
        map_size = dp(self.map_s)
        walls = self.get_walls(map_size)
        ret = astar(self.mv_l, dp(self.start), dp(self.goal), walls, dp(self.pits))
        return ret

    def pub_astar(self, path):
        for step in path:
            msg = AStarPath()
            msg.data = step
            self.pathl_pub.publish(msg)
            rospy.sleep(1)

    def run_mbl(self):
        #iterations = self.max_it 
        iterations = self.max_it
        mb = mbl.MBL(self.prob_f, self.prob_b, self.prob_l, self.prob_r, iterations)
        new_pf, new_pb, new_pl, new_pr = mb.get_probs()
        print "Number of Iterations: " + str(iterations)
        print "Probability of moving forward: " + str(new_pf)
        print "Probability of moving backwards: " + str(new_pb)
        print "Probability of moving left: " + str(new_pl)
        print "Probability of moving right: " + str(new_pr)
        print '\n'
        ml = self.mv_l
        map_size = dp(self.map_s)
        s = dp(self.start)
        g = dp(self.goal)
        w = dp(self.walls)
        p = dp(self.pits)
        md = mdp.MDP(
                    map_size, ml, s, g, w, p, new_pf, new_pb,
                    new_pl, new_pr, self.pit_rwd, self.goal_rwd,
                    self.step_rwd, self.wall_rwd, self.max_it, 
                    self.threshold, self.discount
                 )
        policy = md.get_policy()
        rospy.sleep(1)
        msg = PolicyList()
        d = []
        for row in policy:
            for col in row:
                d.append(col)
        msg.data = d
        rospy.sleep(1)
        self.mbl_pub.publish(msg)
        rospy.sleep(1)
        
        
    def run_mdp(self):
        ml = self.mv_l
        map_size = dp(self.map_s)
        s = dp(self.start)
        g = dp(self.goal)
        w = dp(self.walls)
        p = dp(self.pits)
        md = mdp.MDP(
                    map_size, ml, s, g, w, p, self.prob_f, self.prob_b,
                    self.prob_l, self.prob_r, self.pit_rwd, self.goal_rwd,
                    self.step_rwd, self.wall_rwd, self.max_it, 
                    self.threshold, self.discount
                 )
        policy = md.get_policy()
        rospy.sleep(1)
        msg = PolicyList()
        d = []
        for row in policy:
            for col in row:
                d.append(col)
        msg.data = d
        rospy.sleep(1)
        self.policyl_pub.publish(msg)
        rospy.sleep(1)
    
    def init_map(self):
        new_map = []
        for row in range(self.map_s[0]):
            new_row = []
            for col in range(self.map_s[1]):
                new_row.append(0.0)
            new_map.append(new_row)
        return new_map

    def init_config_vals(self):
        self.config = read_config()
        self.mv_l = self.config["move_list"]
        self.map_s = self.config["map_size"]
        self.start = self.config["start"]
        self.goal = self.config["goal"]
        self.walls = self.config["walls"]
        self.pits = self.config["pits"]
        self.max_it = self.config["max_iterations"]
        self.threshold = self.config["threshold_difference"]
        self.step_rwd = self.config["reward_for_each_step"]
        self.wall_rwd = self.config["reward_for_hitting_wall"]
        self.goal_rwd = self.config["reward_for_reaching_goal"]
        self.pit_rwd = self.config["reward_for_falling_in_pit"]
        self.discount = self.config["discount_factor"]
        self.prob_f = self.config["prob_move_forward"]
        self.prob_b = self.config["prob_move_backward"]
        self.prob_l = self.config["prob_move_left"]
        self.prob_r = self.config["prob_move_right"]

    def get_walls(self, map_size):
        walls = dp(self.config["walls"])
        # Need to add boundaries to the walls
        for row in range(map_size[0]):
            walls.append([row,-1])
            walls.append([row, map_size[1]])
        for col in range(map_size[1]):
            walls.append([-1, col])
            walls.append([map_size[0], col])
        return walls
            
    def init_ros_stuff(self):
        rospy.init_node("robot")
        # Robot's Publications
        self.pathl_pub = rospy.Publisher(
            "/results/path_list",
            AStarPath,
            queue_size = 10
        )
        self.policyl_pub = rospy.Publisher(
            "/results/policy_list",
            PolicyList,
            queue_size = 10
        )
        self.mbl_pub = rospy.Publisher(
            "/results/mbl_policy_list",
            PolicyList,
            queue_size = 10
        )
        self.sc_pub = rospy.Publisher(
            "/map_node/sim_complete",
            Bool,
            queue_size = 10
        )

    def shutdown(self):
        shutdown_msg = Bool()
        shutdown_msg.data = True
        self.sc_pub.publish(shutdown_msg)
        rospy.signal_shutdown("")

if __name__ == '__main__':
    rb = Robot()
