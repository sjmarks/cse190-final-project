"""
Authors
Samuel Marks, A10568175
Bao Liang, A99089908
"""

from copy import deepcopy as dp
from operator import add, sub

"""
Name: mdp
Description: method implementing mdp algorithm
Parameters:
- move_list: list of possible moves
- start: location of start
- goal: location of goal
- walls: location of walls
- pits: location of pits
- map_size: dimensions of the map
- p_f: probability of moving forward
- p_b: probability of moving backward
- p_l: probability of moving left
- p_r: probability of moving right
- pit_reward: reward for hitting pit
- goal_reward: reward for reaching goal
- step_reward: reward for taking a step
- wall_reward: reward for hitting a wall
"""
class MDP():
    def __init__(self, map_size, move_list, start, goal, walls, pits, 
            p_mv_f, p_mv_b, p_mv_l, p_mv_r, pit_reward, goal_reward, 
            step_reward, wall_reward, max_iterations, threshold_diff,
            discount):
        self.map_size = map_size
        self.mv_l = move_list
        self.s = start
        self.g = goal
        self.walls = walls
        self.pits = pits
        self.p_f = p_mv_f
        self.p_b = p_mv_b
        self.p_l = p_mv_l
        self.p_r = p_mv_r
        self.pit_r = pit_reward
        self.goal_r = goal_reward
        self.step_r = step_reward
        self.wall_r = wall_reward
        self.max_i = max_iterations
        self.thresh_d = threshold_diff
        self.df = discount
        # Additional variables generated within MDP
        self.boundaries = self.get_boundaries()
        self.directions = self.get_directions()

    def prob_function(self):
        return 0.0

    def init_val_map(self):    
        old_vals = []
        for row in range(self.map_size[0]):
            new_row = []
            for col in range(self.map_size[1]):
                new_row.append(0.0)
            old_vals.append(new_row)
        old_vals[self.g[0]][self.g[1]] = self.goal_r
        return old_vals

    def get_cell_value(self, old_grid, new_grid, cell):
        val_e = self.eval_cell(old_grid, cell, 'E')
        val_w = self.eval_cell(old_grid, cell, 'W')
        val_s = self.eval_cell(old_grid, cell, 'S')
        val_n = self.eval_cell(old_grid, cell, 'N')
        ret_val = max(val_n, val_w, val_e, val_s)
        new_grid[cell[0]][cell[1]] = ret_val
        if val_n == ret_val:
            return "N"
        if ret_val == val_s:
            return "S"
        if ret_val == val_w:
            return "W"
        if ret_val == val_e:
            return "E"

    def eval_cell(self, old_grid, cell, direction):
        moves = self.directions[direction]
        #Evaluate forward value
        cell_f = map(add, cell, moves['f'])
        if cell_f in self.walls or cell_f in self.boundaries:
            val_f = self.p_f * (self.wall_r + old_grid[cell[0]][cell[1]])
        elif cell_f in self.pits:
            val_f = self.p_f * (self.pit_r + self.step_r)
        else:
            val_f = self.p_f * (self.step_r + old_grid[cell_f[0]][cell_f[1]])
        #Evaluate left value
        cell_l = map(add, cell, moves['l'])
        if cell_l in self.walls or cell_l in self.boundaries:
            val_l = self.p_l * (self.wall_r + old_grid[cell[0]][cell[1]])
        elif cell_f in self.pits:
            val_l = self.p_l * (self.pit_r + self.step_r)
        else:
            val_l = self.p_l * (self.step_r + old_grid[cell_l[0]][cell_l[1]])
        #Evaluate right value
        cell_r = map(add, cell, moves['r'])
        if cell_r in self.walls or cell_r in self.boundaries:
            val_r = self.p_r * (self.wall_r + old_grid[cell[0]][cell[1]])
        elif cell_f in self.pits:
            val_r = self.p_r * (self.pit_r + self.step_r)
        else:
            val_r = self.p_r * (self.step_r + old_grid[cell_r[0]][cell_r[1]])
        #Evaluate backward value
        cell_b = map(add, cell, moves['b'])
        if cell_b in self.walls or cell_b in self.boundaries:
            val_b = self.p_b * (self.wall_r + old_grid[cell[0]][cell[1]])
        elif cell_f in self.pits:
            val_b = self.p_b * (self.pit_r + self.step_r)
        else:
            val_b = self.p_b * (self.step_r + old_grid[cell_b[0]][cell_b[1]])
        return self.df * (val_f + val_b + val_r + val_l)

    def get_policy(self):
        # Create a list of default values
        old_vals = self.init_val_map()
        new_vals = dp(old_vals)
        # Initialize policy grid
        ret_grid = self.init_policy_grid()
        for i in range(self.max_i):
            for row in range(self.map_size[0]):
                for col in range(self.map_size[1]):
                    if [row, col] != self.g and [row, col] not in self.walls + self.pits:
                        ret_grid[row][col] = self.get_cell_value(old_vals, new_vals, [row, col])
            if self.threshold_reached(old_vals, new_vals):
                return ret_grid
            # Store new values obtained into the old_vals
            old_vals = dp(new_vals)
        return ret_grid
    
    def threshold_reached(self, grid1, grid2):
        total = 0.0
        for row in range(self.map_size[0]):
            diff = map(sub, grid1[row], grid2[row])
            for val in diff:
                total += abs(val)
        return total <= self.thresh_d

    def init_policy_grid(self):
        ret_grid = []
        for row in range(self.map_size[0]):
            new_row = []
            for col in range(self.map_size[1]):
                if [row, col] in self.walls:
                    new_row.append("WALL")
                elif [row, col] in self.pits:
                    new_row.append("PIT")
                elif [row, col] == self.g:
                    new_row.append("GOAL")
                else:
                    new_row.append("")
            ret_grid.append(new_row)
        return ret_grid

    def get_boundaries(self):
        boundaries = []
        for row in range(self.map_size[0]):
            boundaries.append([row, -1])
            boundaries.append([row, self.map_size[1]])
        for col in range(self.map_size[1]):
            boundaries.append([-1, col])
            boundaries.append([self.map_size[0], col])
        return boundaries
    
    def get_directions(self):
        # Dictionary Key = move
        #Dictionary values correspond to forward, bac, left, and right 
        direct_dict = {}
        direct_dict['N'] = {'f':[-1,0], 'b':[1,0], 'l':[0,-1], 'r':[0,1]}
        direct_dict['S'] = {'f':[1,0], 'b':[-1,0], 'l':[0,1], 'r':[0,-1]}
        direct_dict['E'] = {'f':[0,1], 'b':[0,-1], 'l':[-1,0], 'r':[1,0]}
        direct_dict['W'] = {'f':[0,-1], 'b':[0,1], 'l':[1,0], 'r':[-1,0]}
        return direct_dict
