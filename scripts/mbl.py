"""
Author
Samuel Marks, A10568175
"""

import random

"""
Name: mbl
Description: class implementing Model-Based learning
"""

class MBL():
    def __init__(self,
            p_mv_f, p_mv_b, p_mv_l, p_mv_r, max_iterations):
        self.true_p_f = p_mv_f
        self.true_p_b = p_mv_b
        self.true_p_l = p_mv_l
        self.true_p_r = p_mv_r
        self.iterations = max_iterations
        # Create new variables for the assumed chance of success
        self.prob_f = 0.0
        self.prob_b = 0.0
        self.prob_l = 0.0
        self.prob_r = 0.0
        self.calc_probs()

    def try_move(self):
        # Generate a random number between 0.0 and 1.0
        num = random.uniform(0, 1)
        move = 0    # Dummy value for n
        # Check against the true move value to see if it's successful
        if num < self.true_p_f:
            move = "f"
        # If move forward fails, continue with other 4 directions
        else :
            num = random.uniform(0,1)
            if num < self.true_p_b/(1.0-self.true_p_f):
                move = "b"
            else :
                num = random.uniform(0,1)
                if num < self.true_p_l/(1.0-self.true_p_f-self.true_p_b):
                    move = "l"
                else:
                    move = "r"
        # Return the move when done
        return move

    """Method to calculate estimated move probabilities"""
    def calc_probs(self):
        # Set a seed create consistent results
        random.seed(0)
        f_total = 0.0
        b_total = 0.0
        l_total = 0.0
        r_total = 0.0
        for i in range(self.iterations):
            move = self.try_move()
            if move == "f" :
                f_total += 1.0
            elif move == "b" :
                b_total += 1.0
            elif move == "l" :
                l_total += 1.0
            else:
                r_total += 1.0
        # Update the probabilities
        self.prob_f = f_total / self.iterations
        self.prob_b = b_total / self.iterations
        self.prob_l = l_total / self.iterations
        self.prob_r = r_total / self.iterations
        
    def get_probs(self):
        # Return the probability for each movement
        return self.prob_f, self.prob_b, self.prob_l, self.prob_r
