#CSE 190 Final Project

Project Author: Samuel Marks

This project is based on recommendation 3 in the assignment instructions.

The goal of the project is to use Model-Based learning in order to be able to find the optimal policies without knowledge of the 
movement probabilities to use in the Markov Decision P algorithm implemented in assignment 3

By default, the MBL algorithm uses the same number of iterations as the iterations parameter in the configuration
file used in assignment 3. To use a different number of iterations the iterations value in the run_mbl() function
has to be changed.
